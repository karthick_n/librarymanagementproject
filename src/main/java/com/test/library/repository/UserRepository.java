package com.test.library.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.test.library.model.DAOUser;

public interface UserRepository extends CrudRepository<DAOUser, Long> {
	@Query("SELECT u FROM DAOUser u WHERE u.username = :username")
    public DAOUser getUserByUsername(@Param("username") String username);
}
