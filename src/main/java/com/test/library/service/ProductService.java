package com.test.library.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.library.entity.Product;
import com.test.library.exception.RecordNotFoundException;
import com.test.library.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	public List<Product> getProductsList() {
		return productRepository.findAll();
	}

	public Product findProductById(Long id) throws RecordNotFoundException {
		Optional<Product> findId = productRepository.findById(id);
		if(findId.isPresent()){
			return findId.get();
		}else{
			throw new RecordNotFoundException("No Product record exist for given id") ;
		}
	}

	public Product updateProduct(Product product) {
		Product existingProduct = productRepository.findById(product.getId()).orElse(null);
		existingProduct.setName(product.getName());
		existingProduct.setPrice(product.getPrice());
		existingProduct.setQuantity(product.getQuantity());
		return productRepository.saveAndFlush(existingProduct);
	}

	public String deleteProductById(Long id) {
		productRepository.deleteById(id);
		return "product removed !! "+id;
	}

	public List<Product> addProductList(List<Product> products) {
		return productRepository.saveAll(products);	
	}

}
