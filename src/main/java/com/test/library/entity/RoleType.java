package com.test.library.entity;
import org.springframework.security.core.GrantedAuthority;

public enum RoleType implements GrantedAuthority {

	ROLE_ADMIN,

	ROLE_MANAGER,

	ROLE_EMPLOYEE;



	public String getAuthority() {
		return name();
	}

}