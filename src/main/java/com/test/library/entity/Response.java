package com.test.library.entity;

public class Response {

	private String res;
	private String description;

	public Response() {
	}

	public Response(String res, String description) {
		super();
		this.res = res;
		this.description = description;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
