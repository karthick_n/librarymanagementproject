package com.test.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.library.entity.Product;
import com.test.library.entity.Response;
import com.test.library.exception.RecordNotFoundException;
import com.test.library.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@PostMapping("/addProduct")
	public ResponseEntity<Product> addProduct(@RequestBody Product product){
		Product prod = productService.addProduct(product);
		return new ResponseEntity<Product>(prod, new HttpHeaders(), HttpStatus.OK);
	}
	@PostMapping("/addProductList")
	public ResponseEntity<List<Product>> addProductList(@RequestBody List<Product> products){
		List<Product> list = productService.addProductList(products);
		return new ResponseEntity<List<Product>>(list, new HttpHeaders(), HttpStatus.OK);
		
	}
	@GetMapping("/getProductsList")
	public ResponseEntity<List<Product>> getProductsList(){
		List<Product> list = productService.getProductsList();
		return new ResponseEntity<List<Product>>(list, new HttpHeaders(), HttpStatus.OK);
		
	}
	@GetMapping("/findProductById/{id}")
	public ResponseEntity<Product> findProductById(@PathVariable Long id) throws RecordNotFoundException{
		Product prod = productService.findProductById(id);
		return new ResponseEntity<Product>(prod, new HttpHeaders(), HttpStatus.OK);	
	}

	@PutMapping("/updateProduct")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product){
		Product prod = productService.updateProduct(product);
		return new ResponseEntity<Product>(prod, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteProductById/{id}")
	public ResponseEntity<Response> deleteProductById(@PathVariable Long id) throws RecordNotFoundException{
		String res = productService.deleteProductById(id);
		Response response = new Response();
		response.setRes(res);
		response.setDescription("Product Data Deleted Successfully....");
		return new ResponseEntity<Response>(response, new HttpHeaders(), HttpStatus.OK);	
	}
	
}
